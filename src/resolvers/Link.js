const votes = (parent, args, context, info) => {
  return context.prisma.link({ 
    id: parent.id 
  }).votes()
}

const postedBy = (root, args, context, info) => {
  return context.prisma.link({
    id: root.id
  }).postedBy()
}

module.exports = {
  votes,
  postedBy,
}