// exports.feed = (root, args, context, info) => context.prisma.links()
// exports.info = () => `This is the API of a Hackernews clone`

const feed = async (root, args, context, info) => {
  const where = args.filter ? {
    OR: [
      { description_contains: args.filter },
      { url_contains: args.filter },
    ],
  } : {}

  const links = await context.prisma.links({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy,
  })

  const count = await context.prisma
    .linksConnection({
      where
    })
    .aggregate()
    .count()
  
  return {
    links,
    count,
  }
}

const info = () => `This is the API of a Hackernews clone`

module.exports = {
  feed,
  info,
}