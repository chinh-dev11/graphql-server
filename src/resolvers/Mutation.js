const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { APP_SECRET, getUserId } = require('../utils')

const vote = async (parent, args, context, info) => {
  const userId = getUserId(context)
  // console.log('userId: ', userId);
  const voteExists = await context.prisma.$exists.vote({
    user: { id: userId },
    link: { id: args.linkId },
  })
  // console.log('voteExists: ', voteExists);
  if (voteExists) {
    throw new Error(`Already voted for link: ${args.linkId}`)
  }

  return context.prisma.createVote({
    user: { connect: { id: userId } },
    link: { connect: { id: args.linkId } },
  })
}

const signup = async (root, args, context, info) => {
  const hashedPassword = await bcrypt.hash(args.password, 10)
  const { password, ...user} = await context.prisma.createUser({
    ...args,
    password: hashedPassword
  })
  const token = jwt.sign({
    userId: user.id,
  }, APP_SECRET)

  return {
    token,
    user
  }
}

const login = async (root, args, context, info) => {
  const { password, ...user } = await context.prisma.user({
    email: args.email
  })
  if(!user) throw Error('No such user found')

  const valid = await bcrypt.compare(args.password, password)
  if(!valid) throw Error('Invalid password')

  const token = jwt.sign({
    userId: user.id
  }, APP_SECRET)

  return {
    token,
    user
  }
}

const post = (root, args, context, info) => {
  const userId = getUserId(context)
  return context.prisma.createLink({
    url: args.url,
    description: args.description,
    postedBy: {
      connect: {
        id: userId
      }
    }
  })
}

module.exports = {
  vote,
  signup,
  login,
  post,
}